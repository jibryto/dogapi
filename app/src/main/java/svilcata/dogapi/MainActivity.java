package svilcata.dogapi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import svilcata.dogapi.Models.Dog;
import svilcata.dogapi.retrofit.ApiInterface;
import svilcata.dogapi.retrofit.ApiUtils;
import svilcata.dogapi.retrofit.Breed_ListHMResponse;
import svilcata.dogapi.retrofit.Breed_RandomImageResponse;

public class MainActivity extends AppCompatActivity {
    private List<Dog> dogList = new ArrayList<>();
    private DogsAdapter dogsAdapter;
    @BindView(R.id.item_list)
    RecyclerView recyclerView;
    @BindView(R.id.indeterminateBar)
    ProgressBar progressBar;
    @BindView(R.id.retrieveList_button)
    Button startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Dogs Gallery");

        dogsAdapter = new DogsAdapter(dogList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dogsAdapter);


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                prepareDog_Adapter();
            }
        });

    }

    private void prepareDog_Adapter() {
        final ApiInterface apiInterface = ApiUtils.getAPIService();
        Call<Breed_ListHMResponse> call = apiInterface.getAllBreeds_ResponseHM();
        call.enqueue(new Callback<Breed_ListHMResponse>() {
            @Override
            public void onResponse(@NonNull Call<Breed_ListHMResponse> call, @NonNull Response<Breed_ListHMResponse> response) {
                for (Map.Entry<String, List<String>> entry : response.body().getMessage().entrySet()) {
                    String name = entry.getKey();
                    name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
                    dogList.add(new Dog(name, entry.getValue(), null));
                }
                updateDogImgURL(dogList);
            }

            @Override
            public void onFailure(@NonNull Call<Breed_ListHMResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void updateDogImgURL(final List<Dog> dogList) {
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ApiUtils.getAPIService();
        for (int i = 0; i < dogList.size(); i++) {
            Call<Breed_RandomImageResponse> call = apiInterface.getBreedRandomImageResponse(dogList.get(i).getBreed());
            final int finalI1 = i;
            call.enqueue(new Callback<Breed_RandomImageResponse>() {
                @Override
                public void onResponse(Call<Breed_RandomImageResponse> call, Response<Breed_RandomImageResponse> response) {
                    dogList.get(finalI1).setmRandomImageURL(response.body().getMessage());
                    dogsAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<Breed_RandomImageResponse> call, Throwable t) {

                }
            });
        }
    }


}